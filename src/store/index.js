import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import initialState from './initialState'
import todos from './todos'
import categories from './categories'
import mySaga from './sagas'

const sagaMiddleware = createSagaMiddleware()
const configureStore = () => {
  let composeEnhancers = compose
  const middlewares = [sagaMiddleware]

  if (process.env.NODE_ENV !== 'production') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    middlewares.push(createLogger())
  }

  return createStore(
    combineReducers({
      todos,
      categories,
    }),
    initialState,
    composeEnhancers(
      applyMiddleware(...middlewares),
    ),
  )
}
const store = configureStore()
sagaMiddleware.run(mySaga)
export default store
