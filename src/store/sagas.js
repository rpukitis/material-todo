import { takeLatest, take, put } from 'redux-saga/effects'
import {
  FETCH_ADD_TODO,
  FETCH_UPDATE_TODO,
  CONFIRM_DELETE_TODO,
  FETCH_DELETE_TODO,
  FETCH_TOGGLE_TODO,
  addTodo,
  updateTodo,
  deleteTodo,
  toggleTodo,
} from './todos'

export function* fetchAddTodo (action) {
  try {
    yield put(addTodo(action.payload))
  } catch (e) {
    console.log(e)
  }
}

export function* fetchUpdateTodo (action) {
  try {
    yield put(updateTodo(action.payload, action.id))
  } catch (e) {
    console.log(e)
  }
}

export function* confirmDeleteTodo (action) {
  try {
    yield take(FETCH_DELETE_TODO)
    yield put(deleteTodo(action.id))
  } catch (e) {
    console.log(e)
  }
}

export function* fetchToggleTodo (action) {
  try {
    yield put(toggleTodo(action.id))
  } catch (e) {
    console.log(e)
  }
}

function* mySaga () {
  yield takeLatest(FETCH_ADD_TODO, fetchAddTodo)
  yield takeLatest(FETCH_UPDATE_TODO, fetchUpdateTodo)
  yield takeLatest(CONFIRM_DELETE_TODO, confirmDeleteTodo)
  yield takeLatest(FETCH_TOGGLE_TODO, fetchToggleTodo)
}

export default mySaga
