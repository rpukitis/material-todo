const content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
const completeBy = new Date('Thu Jan 04 2019 12:00:00 GMT+0200 (EET)')
const initialState = {
  todos: [
    {
      id: 1,
      title: 'ToDo 001',
      category: 'optional',
      done: false,
      completeBy,
    },
    {
      id: 2,
      title: 'ToDo 002',
      category: 'optional',
      done: true,
      content,
    },
    {
      id: 3,
      title: 'ToDo 003',
      category: 'optional',
      done: false,
      completeBy,
    },
    {
      id: 4,
      title: 'ToDo 004',
      category: 'optional',
      done: true,
    },
  ],
}
export default initialState
