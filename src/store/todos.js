export const FETCH_ADD_TODO = 'FETCH_ADD_TODO'
export const FETCH_UPDATE_TODO = 'FETCH_UPDATE_TODO'
export const CONFIRM_DELETE_TODO = 'CONFIRM_DELETE_TODO'
export const FETCH_DELETE_TODO = 'FETCH_DELETE_TODO'
export const FETCH_TOGGLE_TODO = 'FETCH_TOGGLE_TODO'
export const ADD_TODO = 'ADD_TODO'
export const UPDATE_TODO = 'UPDATE_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const TOGGLE_TODO = 'TOGGLE_TODO'

export const fetchAddTodo = payload => ({
  type: FETCH_ADD_TODO,
  payload,
})

export const fetchUpdateTodo = (payload, id) => ({
  type: FETCH_UPDATE_TODO,
  id,
  payload,
})

export const confirmDeleteTodo = id => ({
  type: CONFIRM_DELETE_TODO,
  id,
})

export const fetchDeleteTodo = () => ({
  type: FETCH_DELETE_TODO,
})

export const fetchToggleTodo = id => ({
  type: FETCH_TOGGLE_TODO,
  id,
})

export const addTodo = payload => ({
  type: ADD_TODO,
  payload,
})

export const updateTodo = (payload, id) => ({
  type: UPDATE_TODO,
  id,
  payload,
})

export const deleteTodo = id => ({
  type: DELETE_TODO,
  id,
})

export const toggleTodo = id => ({
  type: TOGGLE_TODO,
  id,
})

let nextID = 100
const todo = (state = {}, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...action.payload,
        id: nextID++,
      }
    case UPDATE_TODO:
      return {
        ...state,
        ...action.payload,
      }
    case TOGGLE_TODO:
      return {
        ...state,
        done: !state.done,
      }
    default:
      return state
  }
}

const todos = (state = [], action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, todo(undefined, action)]
    case UPDATE_TODO:
      return state.map(t => (t.id === action.id ? todo(t, action) : t))
    case DELETE_TODO:
      return state.filter(t => t.id !== action.id)
    case TOGGLE_TODO:
      return state.map(t => (t.id === action.id ? todo(t, action) : t))
    default:
      return state
  }
}
export default todos
