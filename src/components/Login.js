import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import { Route, Redirect } from 'react-router-dom'
import Card from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'

const fakeAuth = {
  isAuthenticated: false,
  authenticate (cb) {
    this.isAuthenticated = true
    setTimeout(cb, 100)
  },
  signout (cb) {
    this.isAuthenticated = false
    setTimeout(cb, 100)
  },
}

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      fakeAuth.isAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location },
          }}
        />
      )
    }
  />
)

const styles = theme => ({
  card: {
    width: 400,
    height: 350,
    marginTop: theme.spacing.unit * 2,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    paddingTop: theme.spacing.unit * 8,
    paddingLeft: theme.spacing.unit * 4,
    paddingRight: theme.spacing.unit * 4,
    paddingBottom: theme.spacing.unit * 2,
  },
  button: {
    width: '100%',
    height: theme.spacing.unit * 8,
    marginTop: theme.spacing.unit * 6,
  },
})

class Login extends Component {
  state = {
    isAuthenticated: fakeAuth.isAuthenticated,
    name: '',
    password: '',
    nameError: false,
    passwordError: false,
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  handleLogin = () => {
    const { name, password } = this.state
    if (name !== 'admin' || password !== 'admin1234') {
      this.setState({
        name: '',
        password: '',
        nameError: name !== 'admin',
        passwordError: password !== 'admin1234',
      })
      return
    }

    fakeAuth.authenticate(() => {
      this.setState({ isAuthenticated: true })
    })
  }

  render () {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { classes } = this.props
    const { isAuthenticated, nameError, passwordError } = this.state

    if (isAuthenticated) {
      return (
        <Redirect to={from} />
      )
    }

    return (
      <Card className={classes.card}>
        <form className={classes.form}>
          <TextField
            error={nameError}
            id="name"
            label="Name"
            value={this.state.name}
            onChange={this.handleChange('name')}
            margin="normal"
          />
          <TextField
            error={passwordError}
            id="password"
            label="Password"
            type="password"
            value={this.state.password}
            onChange={this.handleChange('password')}
            margin="normal"
          />
          <Button
            raised
            color="primary"
            className={classes.button}
            onClick={this.handleLogin}
          >
            Login
          </Button>
        </form>
      </Card>
    )
  }
}
export default withStyles(styles)(Login)
