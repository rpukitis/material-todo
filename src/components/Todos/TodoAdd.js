import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import { Link } from 'react-router-dom'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  textField: {
    flex: 1,
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
  },

  button: {
    margin: 0,
    height: '100%',
    marginBottom: theme.spacing.unit * 1,
    marginRight: theme.spacing.unit * 2,
  },
})

class AddTodo extends Component {
  state = {
    value: '',
  }

  handleChange = (e) => {
    this.setState({ value: e.target.value })
  }

  render () {
    const { classes } = this.props
    const { value } = this.state
    return (
      <form className={classes.root}>
        <TextField
          required
          id="Title"
          label="Title"
          className={classes.textField}
          value={value}
          onChange={this.handleChange}
          margin="normal"
        />
        {value ? (
          <Link
            className={classes.button}
            to={{
              pathname: '/todo',
              state: { title: value },
            }}
          >
            <Button raised color="primary">
              Add
            </Button>
          </Link>
        ) : (
          <Button raised color="accent" disabled className={classes.button}>
            Add
          </Button>
        )}
      </form>
    )
  }
}

export default withStyles(styles)(AddTodo)
