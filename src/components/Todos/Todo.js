import React, { Component } from 'react'
import Switch from 'material-ui/Switch'
import IconButton from 'material-ui/IconButton'
import Avatar from 'material-ui/Avatar'
import AssignmentIcon from 'material-ui-icons/Assignment'
import DeleteIcon from 'material-ui-icons/Delete'
import List, {
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from 'material-ui/List'
import Collapse from 'material-ui/transitions/Collapse'
import ExpandLess from 'material-ui-icons/ExpandLess'
import ExpandMore from 'material-ui-icons/ExpandMore'
import red from 'material-ui/colors/red'
import purple from 'material-ui/colors/purple'
import blue from 'material-ui/colors/blue'
import green from 'material-ui/colors/green'
import { withStyles } from 'material-ui/styles'
import { prettifyDate } from '../../services'

const styles = theme => ({
  urgent: {
    color: '#fff',
    backgroundColor: red[500],
  },
  important: {
    color: '#fff',
    backgroundColor: purple[500],
  },
  normal: {
    color: '#fff',
    backgroundColor: blue[500],
  },
  optional: {
    color: '#fff',
    backgroundColor: green[500],
  },
})

const Content = ({ text, open }) => {
  return text ? (
    <Collapse
      component="li"
      in={open}
      timeout="auto"
      unmountOnExit
    >
      <List disablePadding>
        <ListItem button>
          <ListItemText inset primary={text} />
        </ListItem>
      </List>
    </Collapse>
  ) : null
}

class Todo extends Component {
  state = {
    open: false,
  }

  handleCollapseOpen = () => {
    this.setState({ open: !this.state.open })
  }

  render () {
    const {
      title,
      content,
      done,
      completeBy,
      category,
      classes,
      handleClick,
      handleToggle,
      handleConfirmDelete,
    } = this.props
    const open = this.state.open

    return [
      <ListItem key="item" button onClick={handleClick}>
        <Avatar className={classes[category]}>
          <AssignmentIcon />
        </Avatar>
        <ListItemText
          primary={title}
          secondary={completeBy && prettifyDate(completeBy)}
        />
        <ListItemSecondaryAction>
          {content && (
            <IconButton aria-label="Delete" onClick={this.handleCollapseOpen}>
              {this.state.open ? <ExpandLess /> : <ExpandMore />}
            </IconButton>
          )}
          <Switch checked={done} onChange={handleToggle} />
          <IconButton aria-label="Delete" onClick={handleConfirmDelete}>
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>,
      <Content key="content" text={content} open={open} />,
    ]
  }
}

export default withStyles(styles)(Todo)
