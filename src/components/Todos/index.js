import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from 'material-ui/List'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Input, { InputAdornment } from 'material-ui/Input'
import Radio, { RadioGroup } from 'material-ui/Radio'
import { FormLabel, FormControl, FormControlLabel } from 'material-ui/Form'
import Dialog, { DialogActions, DialogTitle } from 'material-ui/Dialog'
import Button from 'material-ui/Button'
import Search from 'material-ui-icons/Search'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import {
  fetchToggleTodo,
  confirmDeleteTodo,
  fetchDeleteTodo,
} from '../../store/todos'
import { convertDateToString, getVisibleTodos } from '../../services'
import TodoAdd from './TodoAdd'
import Todo from './Todo'

const styles = theme => ({
  root: {
    flex: 1,
    maxWidth: 900,
  },
  typography: {
    paddingLeft: theme.spacing.unit * 2,
  },
  card: {
    marginTop: theme.spacing.unit,
  },
  content: {
    paddingBottom: theme.spacing.unit,
  },
  list: {
    maxHeight: 500,
    overflowY: 'scroll',
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '100%',
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit,
  },
  group: {
    flexDirection: 'row',
  },
})

class Todos extends Component {
  static defaultProps = {
    todos: [],
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  state = {
    search: '',
    filter: 'all',
    confirmOpen: false,
  }

  handleClick = (id, state) => () => {
    this.props.history.push({
      pathname: `/todo/${id}`,
      state: {
        ...state,
        completeBy: convertDateToString(state.completeBy),
        id,
      },
    })
  }

  handleToggle = id => (event, checked) => {
    this.props.fetchToggleTodo(id)
    return !checked
  }

  handleConfirmDelete = id => () => {
    this.setState({ confirmOpen: true })
    this.props.confirmDeleteTodo(id)
  }

  handleCancelDelete = () => {
    this.setState({ confirmOpen: false })
  }

  handleDelete = () => {
    this.setState({ confirmOpen: false })
    this.props.fetchDeleteTodo()
  }

  handleSearch = e => {
    this.setState({ search: e.target.value })
  }

  handleFilter = (event, value) => {
    this.setState({ filter: value })
  }

  render () {
    const { classes, todos } = this.props
    const { filter, search, confirmOpen } = this.state
    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <CardContent className={classes.content}>
            <Typography
              type="display1"
              gutterBottom
              className={classes.typography}
            >
              Todos
            </Typography>
            <TodoAdd />
            <List className={classes.list}>
              {getVisibleTodos(todos, filter, search).map(todo => (
                <Todo
                  key={todo.id}
                  {...todo}
                  handleClick={this.handleClick(todo.id, todo)}
                  handleToggle={this.handleToggle(todo.id)}
                  handleConfirmDelete={this.handleConfirmDelete(todo.id)}
                />
              ))}
            </List>
          </CardContent>
          <CardActions className={classes.actions}>
            <FormControl component="fieldset">
              <FormLabel component="legend">Type</FormLabel>
              <RadioGroup
                aria-label="filter"
                name="filter"
                className={classes.group}
                value={this.state.filter}
                onChange={this.handleFilter}
              >
                <FormControlLabel value="all" control={<Radio />} label="All" />
                <FormControlLabel
                  value="active"
                  control={<Radio />}
                  label="Active"
                />
                <FormControlLabel
                  value="completed"
                  control={<Radio />}
                  label="Completed"
                />
              </RadioGroup>
            </FormControl>
            <FormControl>
              <Input
                id="adornment-password"
                type="text"
                value={this.state.search}
                onChange={this.handleSearch}
                startAdornment={
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                }
              />
            </FormControl>
          </CardActions>
        </Card>
        <Dialog
          open={confirmOpen}
          maxWidth="xs"
          aria-labelledby="confirmation-dialog-title"
        >
          <DialogTitle id="confirmation-dialog-title">
            Are you sure you want to delete todo entry?
          </DialogTitle>
          <DialogActions>
            <Button onClick={this.handleCancelDelete} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleDelete} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

const stateToProps = state => ({
  todos: state.todos,
})

const dispatchToProps = {
  fetchToggleTodo,
  confirmDeleteTodo,
  fetchDeleteTodo,
}

export default withStyles(styles)(
  withRouter(connect(stateToProps, dispatchToProps)(Todos))
)
