import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import withRoot from '../withRoot'
import Todo from './Todo'
import Todos from './Todos'
import Login, { PrivateRoute } from './Login'

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    minHeight: '100vh',
    backgroundColor: '#eee',
  },
})

class Index extends Component {
  render () {
    const { classes } = this.props
    return (
      <Router>
        <div className={classes.root}>
          <Switch>
            <PrivateRoute exact path="/" component={Todos} />
            <PrivateRoute path="/todo/:id" component={Todo} />
            <PrivateRoute path="/todo" component={Todo} />
            <Route path="/login" component={Login} />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default withRoot(withStyles(styles)(Index))
