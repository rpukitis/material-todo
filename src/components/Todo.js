import React, { Component } from 'react'
import Card from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import Input, { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import { FormControl, FormControlLabel } from 'material-ui/Form'
import Select from 'material-ui/Select'
import Switch from 'material-ui/Switch'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import { withStyles } from 'material-ui/styles'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { fetchAddTodo, fetchUpdateTodo } from '../store/todos'
import withRoot from '../withRoot'
import { pick, convertStringToDate } from '../services'

const styles = theme => {
  const padding = theme.spacing.unit * 2
  return {
    root: {
      width: '100%',
      maxWidth: 900,
    },
    typography: {
      paddingLeft: padding,
      paddingTop: padding,
    },
    card: {
      marginTop: theme.spacing.unit,
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      paddingTop: padding,
      paddingBottom: padding,
    },
    formControl: {
      marginLeft: padding,
      marginRight: padding,
      '&:not(:first-child)': {
        marginTop: padding,
      },
    },
    formControlButtons: {
      marginLeft: padding,
      marginRight: padding,
      paddingTop: padding,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
    },
    controlButtons: {
      width: '40%',
      '&:not(:first-child)': {
        marginLeft: padding,
      },
    },
    textField: {
      margin: 0,
    },
  }
}

class Todo extends Component {
  state = {
    title: '',
    content: '',
    category: 'optional',
    completeBy: '',
    done: false,
    ...this.props.location.state,
  }

  handleChange = (type) => (e) => {
    this.setState({ [type]: e.target.value })
  }

  handleCancel = () => {
    this.props.history.push({ pathname: '/' })
  }

  handleConfirm = () => {
    const { history, fetchAddTodo, fetchUpdateTodo } = this.props
    const handler = this.state.id ? fetchUpdateTodo : fetchAddTodo
    const payload = {
      ...pick(['title', 'content', 'category', 'done'], this.state),
      completeBy: convertStringToDate(this.state.completeBy),
    }
    handler(payload, this.state.id)
    history.push({ pathname: '/' })
  }

  render () {
    const { classes, categories } = this.props
    const { id, title, content, category, completeBy, done } = this.state

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <Typography type="display1" gutterBottom className={classes.typography}>
            { id ? 'Edit Todo' : 'Add Todo'}
          </Typography>
          <form className={classes.form}>
            <FormControl className={classes.formControl}>
              <TextField
                required
                id="Title"
                label="Title"
                className={classes.textField}
                value={title}
                onChange={this.handleChange('title')}
                margin="normal"
              />
            </FormControl>

            <FormControl className={classes.formControl}>
              <TextField
                multiline
                id="Content"
                label="Content"
                className={classes.textField}
                value={content}
                onChange={this.handleChange('content')}
                margin="normal"
              />
            </FormControl>

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="todo-category">Category</InputLabel>
              <Select
                value={category}
                onChange={this.handleChange('category')}
                input={<Input name="category" id="todo-category" />}
              >
                {
                  categories.map(c =>
                    <MenuItem key={c} value={c}>{c}</MenuItem>
                  )
                }
              </Select>
            </FormControl>

            <FormControl className={classes.formControl}>
              <TextField
                id="completeBy"
                label="Complete by"
                type="date"
                defaultValue={completeBy}
                onChange={this.handleChange('completeBy')}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormControl>

            <FormControlLabel
              className={classes.formControl}
              control={
                <Switch
                  checked={done}
                  onChange={(event, checked) => this.setState({ done: checked })}
                />
              }
              label="Completed"
            />

            <FormControl className={classes.formControlButtons}>
              <Button
                raised
                color="accent"
                className={classes.controlButtons}
                onClick={this.handleCancel}
              >
                Cancel
              </Button>
              <Button
                raised
                disabled={!title}
                color="primary"
                className={classes.controlButtons}
                onClick={this.handleConfirm}
              >
                { id ? 'Edit' : 'Add'}
              </Button>
            </FormControl>

          </form>
        </Card>
      </div>
    )
  }
}

const stateToProps = (state) => ({
  categories: state.categories,
})

const dispatchToProps = {
  fetchAddTodo,
  fetchUpdateTodo,
}

export default withRoot(
  withStyles(styles)(
    withRouter(
      connect(stateToProps, dispatchToProps)(Todo)
    )
  )
)
