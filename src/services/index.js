export const pick = (keys, obj) => keys.reduce((acc, k) => {
  acc[k] = obj[k]
  return acc
}, {})

export const zeroPad = (number, size) => {
  number = number.toString()
  while (number.length < size) {
    number = '0' + number
  }
  return number
}

export const search = (array, prop, key) => {
  if (!key) {
    return array
  }

  const keyLowerCase = key.toLowerCase()
  return array.filter(object => {
    const found = object[prop]
      .toLowerCase()
      .search(keyLowerCase) !== -1
    return found
  })
}

export const getVisibleTodos = (todos, filter, key) => {
  switch (filter) {
    case 'all':
      return search(todos, 'title', key)
    case 'completed':
      return search(todos.filter(t => t.done), 'title', key)
    case 'active':
      return search(todos.filter(t => !t.done), 'title', key)
    default:
      throw new Error(`Unknown filter: ${filter}.`)
  }
}

export const convertDateToString = (value) => {
  if (!value) {
    return ''
  }
  const year = value.getFullYear()
  const month = zeroPad(value.getMonth() + 1, 2)
  const day = zeroPad(value.getDay(), 2)
  return `${year}-${month}-${day}`
}

export const convertStringToDate = (value) => {
  return value ? new Date(
    ...value
      .trim()
      .split('-')
      .map(x => parseInt(x, 10))
      .reduce((acc, x, i) => {
        acc.push(i === 1 ? x - 1 : x)
        return acc
      }, [])
  ) : null
}

export const prettifyDate = (date) => {
  const MONTH = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
  ]
  return `${MONTH[date.getMonth()]} ${date.getDay()}  ${date.getFullYear()}`
}
