import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import Index from './components/index'
import store from './store'

const App = () => (
  <Provider store={store}>
    <Index />
  </Provider>
)

ReactDOM.render(<App />, document.querySelector('#root'))
