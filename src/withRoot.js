import React from 'react'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import Reboot from 'material-ui/Reboot'

const withRoot = (Component) => (props) => (
  <MuiThemeProvider theme={createMuiTheme()}>
    <Reboot />
    <Component {...props} />
  </MuiThemeProvider>
)

export default withRoot
